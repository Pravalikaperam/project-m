import axios from 'axios'

import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE
} from './userTypes'

export const fetchUsers = () => {
  return (dispatch) => {
    console.log("in the action")
    const access_token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTUzNTA3NzMsIm5iZiI6MTYxNTM1MDc3MywianRpIjoiYzA5NGIwYjctZTAyMy00Zjk1LWIyZjEtMmVjMWRiY2Y4YjQ1IiwiZXhwIjoxNjE1MzYyNzczLCJpZGVudGl0eSI6IntcImNvbXBhbnlcIjogbnVsbCwgXCJuYW1lXCI6IFwiU2Fpc3lhbVwiLCBcInByb2ZpbGVfdXJsXCI6IG51bGwsIFwidXNlcm5hbWVcIjogXCJzYWlzeWFtQG1vYmlnZXN0dXJlLmNvbVwiLCBcImlkXCI6IDEsIFwicm9sZVwiOiB7XCJpZFwiOiAxLCBcIm5hbWVcIjogXCJIUiBNYW5hZ2VyXCIsIFwicHJpdmlsZWdlXCI6IDEwNDg1NzUsIFwiaXNva2V5XCI6IFwiSFJNQU5BR0VSXCJ9fSIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.QLa2XEWpOFeb093vDBZQk5wNPW0WODSkN-3WdH9Uxn0";
    dispatch(fetchUsersRequest())
    axios.get('http://ec2-52-87-201-103.compute-1.amazonaws.com:5000/list_jd?page_no=1&rows_per_page=3&status=inprogress',{
        headers:{
          'Authorization':`Bearer ${access_token}`,
        }
      })
      .then(response => {
        const users = response.data.data;
        console.log("data fetched",users, response);
        dispatch(fetchUsersSuccess(users))
      })
      .catch(error => {
        dispatch(fetchUsersFailure(error.message))
      })
  }
}

export const fetchUsersRequest = () => {
  return {
    type: FETCH_USERS_REQUEST
  }
}

export const fetchUsersSuccess = users => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users
  }
}

export const fetchUsersFailure = error => {
  return {
    type: FETCH_USERS_FAILURE,
    payload: error
  }
}