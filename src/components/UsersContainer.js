import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import './footer.css'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchUsers } from './userActions'
import logo from './download.jpeg'


const useStyles = makeStyles((theme) => ({
  root: {
    marginRight: 50,
    width: "100%",
    backgroundColor: "light",
  },
  appbar: {
    backgroundColor: "#17315E",
    color: "white",
  },
  title: {
    flexGrow: 3,
    color: "white",
    marginLeft: 0,
    paddingRight: 670,
  },
  title2: {
    paddingLeft: "50px",
  },
  cards: {
    display: "flex",
    flexwrap: "wrap",
    width: "80%",
    margin: "auto",
    paddingLeft: "-5px",
    paddingTop: "20px",
    boxShadow: "5px",
    // justifyContent:"right",
    textAlign:"left"
  },
  pos: {
    marginBottom: 12,
  },
  toolbarButtons: {
    marginLeft: 'auto',
    flexGrow: 0.3,
    color: "white",
  },
  connect: {
    backgroundColor: "silver"
  },
  jobs: {
    paddingRight: 1250,
  },
  content:{
    justifyContent:"flex-start",
  },
}));

function UsersContainer({ userData, fetchUsers }) {
  const classes = useStyles();
  useEffect(() => {
    fetchUsers()
  }, [])
  return (
    userData.loading ? (
      <h2>Loading</h2>
    ) : userData.error ? (
      <h2>{userData.error}</h2>
    ) : (
          <div>
            <div className={classes.root}>
              <AppBar position="fixed" className={classes.appbar}>
                <Toolbar>
                 
                  <Typography variant="h6" className={classes.title}>
                    Mobigesture
                  </Typography>
                  <Typography className="topnav-right">
                    <b>Services</b>
                  </Typography>
                  <Typography className="topnav-right">
                    <b>About us</b>
                  </Typography>
                  <Typography className="topnav-right">
                    <b>Careers</b>
                  </Typography>
                  <Typography className="topnav-right">
                    <b>Contact us</b>
                  </Typography>
                  <button class="talk"><b>Let's Talk</b></button>
                </Toolbar>
              </AppBar>
            </div>
            <div className={classes.jobs}>
              <h2 className="underline"> Jobs</h2>
            </div>
            <div className={classes.cards}>
              {
                userData &&
                userData.users &&
                userData.users.map(user => (
                  <Card className={classes.root} variant="outlined">
                    <CardContent justifyContent="flex-start">
                      <p><b>{user.client.name}</b></p>
                      <p><b>Date:</b>{user.expiry_date}</p>
                      <p><b>Description:</b>{user.description}</p>
                      <p><b>Location:</b>{user.location}</p>
                    </CardContent>
                    <Divider />
                    <CardActions>
                      <Button size="small" className={classes.appbar}>Apply for job</Button>
                      <Button size="small" className={classes.appbar}>Reffer Frined</Button>
                      <Button size="small" className={classes.appbar}>Details</Button>
                    </CardActions>

                  </Card>
                ))
              }
            </div>
            <div>
              <footer className="main-footer">
                <div>
                  <h4>MOBIGESTURE</h4>
                  <p className="stay" >Privacy Policy | Terms of Use | Copyright © 2019 Mobigesture.<br></br>All Rights Reserved.</p>
                </div>
                <div>
                  <ul className="footerlist li">
                    <h4>Services</h4>
                    <li>Modernization</li>
                    <li>Device visualization</li>
                    <li>Device Management</li>
                  </ul>
                </div>
                <div>
                  <ul className="footerlist li">
                    <h4>Company</h4>
                    <li>About</li>
                    <li>Contact us</li>
                    <li>Careers</li>
                  </ul>
                </div>
                <div>
                  <h4 className="stay">Stay tuned</h4>
                  <Button className="btn" variant="contained" size="medium" color="primary" href="#contained-buttons">
                    LinkedIn
                    </Button>
                </div>
              </footer>
            </div>
          </div >
        )
  )
}

const mapStateToProps = state => {
  return {
    userData: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchUsers: () => dispatch(fetchUsers())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersContainer)