import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux'
import UsersContainer from './components/UsersContainer';
import store from './components/store'



function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <UsersContainer />
        
      </div>
    </Provider>

  );
}

export default App;
